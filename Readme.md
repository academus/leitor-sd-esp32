# Lendo arquivo no cartão SD com Esp32

Nessessário o uso da classe mySD.h para que possa ser feita a mudança de leitura das portas SPI convencionais para a porta SPI2.
Download: https://github.com/nhatuan84/esp32-micro-sdcard

**Objetivo:**

Leitura de dados em um cartão SD conectado a seguda SPI do Esp32 30 pinos.
Para realizar a leitura foi testado mais de um método para pegar dados linha a linha e realizado uma comparação, porém o método mais eficiente foi definir o tamanho do buffer de leitura de acordo com o número de caracteres em cada linha. O buffer tem de ter o tamanho completo até a quebra de linha "\n".
Para um vetor de 8 caracters, é necessário definir um buffer de tamanho nove, onde engloba todos os caracteres mais a quebra de linha, utilizando assim o método "myFile.read" sempre com tamanho fixo.
Foi testado o método "myFile.readBytesUntil", porém em 50k linhas o tempo aumentava em quase 1 segundo, e em 200k linhas em pelo menos 2 segundos, já que a leitura é byte a byte, até encontrar o \n.


## Portas:

<table>
<tr><td>Portas</td><td>Numero</td></tr>
<tr><td>CS</td><td>25</td></tr>
<tr><td>MISO</td><td>33</td></tr>
<tr><td>MOSI</td><td>13</td></tr>
<tr><td>SCK</td><td>14</td></tr>
</table>

## Pinout Esp32
https://randomnerdtutorials.com/esp32-pinout-reference-gpios/

**Esp32:**

![Esp32 30 pinos](ESP32-Pinout.jpg)

**Leitor SD SPI**

![Micro SD Read/Record](sdspi.jpg)


## Hardware
 - Esp32 30 pinos
 - Leitor SD SPI

## Métodos de Leitura

**read()**
https://www.arduino.cc/reference/en/language/functions/communication/stream/streamread/

**readUntil()**
https://www.arduino.cc/reference/en/language/functions/communication/serial/readbytesuntil/