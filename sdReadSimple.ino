/*
 * Leitor SD utilizando HSPI(SPI2)
 * Do Esp32
 */
 
#include <SPI.h>
#include <mySD.h>

//store line text with 8 chars+/n
//the number is calculate numbers of chars+1, ignore 0
uint8_t buffer[9];

//initiate file to be manipulated
File myFile;

//here we will define the ports to use HSPI on Esp32
//pin 12(MISO) was changed to 33, because Esp can't start with something connected on this port
const int HCS = 25;
const int HMISO = 33;
const int HMOSI = 13;
const int HSCK = 14;

void setup()
{
 // Open serial communications and wait for port to open:
  Serial.begin(115200);
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  Serial.print("Initializing SD card...");
   pinMode(SS, OUTPUT);
   
  if (!SD.begin(HCS, HMOSI, HMISO, HSCK)) {
    Serial.println("initialization failed!");
    ESP.restart();
    return;
  }
  Serial.println("initialization done.");
  
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.

  myFile = SD.open("dados.txt");
  if (myFile) {
    Serial.println("dados.txt:");
    
    //Variables to count necessary time to read lines in file
    unsigned long startTime = millis();
    unsigned long endTime;


    int l;
    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      l = myFile.read(buffer, sizeof(buffer));
      //l = myFile.readBytes(buffer, sizeof(buffer));
      
      //Set end of variable before /n to get only valid information
      buffer[l-1] = 0;
      if (strcmp((char*) buffer, "B91F5B88") == 0) {
        Serial.println("Found!");
      }
      //Serial.print(F((char*) buffer));
      //Serial.println(";");

    }
    //print time necessary to read all lines
    //to 50k line in test, did something like 1700 mills
    endTime = millis() - startTime;
    Serial.println(endTime);
    // close the file:
    myFile.close();

    Serial.println("Fim da Leitura");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }

  
}

void loop()
{
	// nothing happens after setup
}
